import java.util.UUID;
public class CD extends LibraryItem{
    
    private String uuid;
    private User borrower;

    public CD(String name, boolean isBorrowable, boolean isBorrowed){
        super(name, isBorrowable, isBorrowed);
        this.uuid = UUID.randomUUID().toString();
    }

    //Constructor to setup CD which is impossible to borrow
    public CD(String name){
        super(name);
        this.uuid = UUID.randomUUID().toString(); 
    }

    @Override
    public void borrowToUser(User borrower){
        this.borrower = borrower;
    }

    @Override
    public void returnToLibrary(){
        this.borrower = null;
    }

    @Override
    public User getBorrower(){
        return this.borrower;
    }

}