public abstract class LibraryItem {
    
    public String name;
    public boolean isBorrowable = false;
    public boolean isBorrowed = false;

    public LibraryItem(String name, boolean isBorrowable, boolean isBorrowed){
        this.name = name;
        this.isBorrowable = isBorrowable;
        this.isBorrowed = isBorrowed;
    }

    public LibraryItem(String name){
        this.name = name;
    }

    public void borrowToUser(User borrower){
    }

    public void returnToLibrary(){
    }

    public User getBorrower(){return null;}

}