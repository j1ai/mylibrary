import java.util.UUID;
public class Periodical extends LibraryItem{
    
    private String uuid;

    public Periodical(String name){
        super(name);
        this.uuid = UUID.randomUUID().toString(); 
    }

}