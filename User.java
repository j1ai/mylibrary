import java.util.ArrayList;

import java.util.UUID;
public class User {
    
    private String uuid;
    private String name;
    private int borrowLimit;

    public User(String name, int borrowLimit){
        this.name = name;
        this.uuid = UUID.randomUUID().toString();
        this.borrowLimit = borrowLimit;
    }

    public int getUserBorrowLimit(){
        return borrowLimit;
    }

    public void setUserBorrowLimit(int borrowLimit){
        this.borrowLimit = borrowLimit;
    }

    public String getUUID(){
        return this.uuid;
    }
}