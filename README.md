# README #

This is a project about the structure of a library.

Each Library has 3 attributes: name, libraryItems, borrowedItems.
name: Name of this library
libraryItems: List of Library items in this library
borrowedItems: List of borrowed Library Items in this library.

Library item is a parent class of the following Library items: DVD, Book, CD, and Periodical
And each Library Item has 5 attributes: name, isBorrowable, isBorrowed, uuid, borrower
name: Name of this Library Item
isBorrowable: Boolean variable indicates whether this item is borrowable to the user
isBorrowed: Boolean variable indicates whether this item is being borrowed
uuid: Unique ID assigned to this library item
borrower: User who borrowed this library item

User is a class who can borrow library items from the Library
Each User has 3 attributes: name, uuid, borrowLimit
name: Name of this user
uuid: Unique ID assigned to this user
borrowLimit: Borrowing Limit for this user

Library has 2 main methods to allow itself to perform these 2 daily operations: Borrowing Library Item to User, Returning Library Item to the Library. 
The 2 methods are borrowItemToUser() and returnItemToLibrary()