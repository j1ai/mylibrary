import java.util.UUID;
public class Book extends LibraryItem{
    
    private String uuid;
    private User borrower;

    public Book(String name, boolean isBorrowable, boolean isBorrowed){
        super(name, isBorrowable, isBorrowed);
        this.uuid = UUID.randomUUID().toString();
    }

    //Constructor to setup book which is impossible to borrow
    public Book(String name){
        super(name);
        this.uuid = UUID.randomUUID().toString(); 
    }

    @Override
    public void borrowToUser(User borrower){
        this.borrower = borrower;
    }

    @Override
    public void returnToLibrary(){
        this.borrower = null;
    }

    @Override
    public User getBorrower(){
        return this.borrower;
    }

}