import java.util.ArrayList;

public class Library{
    
    public String libraryName;
    private ArrayList<LibraryItem> libraryItems;
    private ArrayList<LibraryItem> borrowedItems;

    public Library(String libraryName){
        this.libraryName = libraryName;
        this.libraryItems = new ArrayList<LibraryItem>();
        this.borrowedItems = new ArrayList<LibraryItem>();
    }

    public void addItemToLibrary(LibraryItem item){
        this.libraryItems.add(item);
    }
    
    public void removeItemFromLibrary(LibraryItem item) throws Exception{
        if (!libraryItems.contains((item))){
            throw new Exception("Item is not found in this library!");
        }
        if (!borrowedItems.contains((item))){
            throw new Exception("Item is being borrowed by a User!");
        }
        libraryItems.remove(item);
    }

    public void borrowItemToUser(LibraryItem item, User borrower) throws Exception{
        int userBorrowLimit = borrower.getUserBorrowLimit();
        if (userBorrowLimit > 0){
            if (item.isBorrowable){
                borrowedItems.add(item);
                item.borrowToUser(borrower);
                userBorrowLimit -= 1;
                borrower.setUserBorrowLimit(userBorrowLimit);
            }
            else{
                throw new Exception("Item is not borrowable in this library!");
            }
            if (!libraryItems.contains((item))){
                throw new Exception("Item is not found in this library!");
            }
            if (borrowedItems.contains((item))){
                throw new Exception("Item is being borrowed to another User!");
            }
        }
        else{
            throw new Exception("This user has exceeded his/her borrow limit!");
        }
    }

    public void returnItemToLibrary(LibraryItem item, User returner) throws Exception{
        String borrowerUUID = item.getBorrower().getUUID();
        if (returner.getUUID().equals(borrowerUUID)){
            if (!borrowedItems.contains((item))){
                throw new Exception("Item is not being borrowed by anyone!");
            }
            if (!libraryItems.contains((item))){
                throw new Exception("Item is not found in this library!");
            }
            if (item.isBorrowable){
                borrowedItems.remove(item);
                item.returnToLibrary();
                returner.setUserBorrowLimit(returner.getUserBorrowLimit()-1);
            }
            else{
                throw new Exception("Item is not borrowable in this library!");
            }
        }
        else{
            throw new Exception("Item's borrower does not match the returner!");
        }
    }

}